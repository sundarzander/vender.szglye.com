# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180103094050) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_roles", id: false, force: :cascade do |t|
    t.integer "admin_id", null: false
    t.integer "role_id",  null: false
    t.index ["admin_id", "role_id"], name: "index_admin_roles_on_admin_id_and_role_id", unique: true, using: :btree
    t.index ["admin_id"], name: "index_admin_roles_on_admin_id", using: :btree
    t.index ["role_id"], name: "index_admin_roles_on_role_id", using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "username",               default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["username"], name: "index_admins_on_username", unique: true, using: :btree
  end

  create_table "agencies", force: :cascade do |t|
    t.string   "name",       null: false
    t.text     "intro"
    t.string   "letter",     null: false
    t.string   "key",        null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_agencies_on_key", using: :btree
  end

  create_table "agency_exposures", force: :cascade do |t|
    t.string   "title",                                                           null: false
    t.string   "app",                                                             null: false
    t.string   "cpm",                                                             null: false
    t.string   "channel",                                                         null: false
    t.string   "agency",                                                          null: false
    t.string   "start_at",                                                        null: false
    t.integer  "admin_id",                                                        null: false
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.string   "price"
    t.string   "#<ActiveRecord::ConnectionAdapters::PostgreSQL::TableDefinition"
    t.index ["admin_id"], name: "index_agency_exposures_on_admin_id", using: :btree
  end

  create_table "agency_tasks", force: :cascade do |t|
    t.decimal  "price",           precision: 8, scale: 2, default: "0.0", null: false
    t.integer  "amount",                                  default: 0,     null: false
    t.integer  "click_amount",                            default: 0,     null: false
    t.integer  "callback_amount",                         default: 0,     null: false
    t.integer  "task_id",                                                 null: false
    t.integer  "agency_id",                                               null: false
    t.date     "create_date",                                             null: false
    t.boolean  "enabled",                                 default: false, null: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.integer  "admin_id"
    t.integer  "app_id"
    t.integer  "cpa",                                     default: 0
    t.integer  "cpc",                                     default: 0
    t.boolean  "ok",                                      default: false
    t.index ["admin_id"], name: "index_agency_tasks_on_admin_id", using: :btree
    t.index ["agency_id"], name: "index_agency_tasks_on_agency_id", using: :btree
    t.index ["app_id"], name: "index_agency_tasks_on_app_id", using: :btree
    t.index ["task_id"], name: "index_agency_tasks_on_task_id", using: :btree
  end

  create_table "apps", force: :cascade do |t|
    t.string   "title",                     null: false
    t.string   "image",                     null: false
    t.integer  "app_store_id",              null: false
    t.string   "app_store_url",             null: false
    t.integer  "app_type",      default: 0, null: false
    t.text     "intro"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["app_store_id"], name: "index_apps_on_app_store_id", using: :btree
  end

  create_table "channel_exposures", force: :cascade do |t|
    t.string   "title",                                                           null: false
    t.string   "app",                                                             null: false
    t.string   "cpm",                                                             null: false
    t.string   "channel",                                                         null: false
    t.string   "start_at",                                                        null: false
    t.string   "end_at",                                                          null: false
    t.integer  "admin_id",                                                        null: false
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.string   "price"
    t.string   "#<ActiveRecord::ConnectionAdapters::PostgreSQL::TableDefinition"
    t.index ["admin_id"], name: "index_channel_exposures_on_admin_id", using: :btree
  end

  create_table "channels", force: :cascade do |t|
    t.string   "name",       null: false
    t.text     "intro"
    t.string   "letter",     null: false
    t.string   "key",        null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_channels_on_key", using: :btree
  end

  create_table "permissions", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_name",              null: false
    t.string   "ancestry"
    t.integer  "ancestry_depth", default: 0, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "role_permissions", id: false, force: :cascade do |t|
    t.integer "role_id",       null: false
    t.integer "permission_id", null: false
    t.index ["permission_id"], name: "index_role_permissions_on_permission_id", using: :btree
    t.index ["role_id", "permission_id"], name: "index_role_permissions_on_role_id_and_permission_id", unique: true, using: :btree
    t.index ["role_id"], name: "index_role_permissions_on_role_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "screenshots", force: :cascade do |t|
    t.string   "image",      null: false
    t.integer  "app_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["app_id"], name: "index_screenshots_on_app_id", using: :btree
  end

  create_table "settings", force: :cascade do |t|
    t.string   "title",       null: false
    t.string   "description", null: false
    t.string   "domain_name", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "task_amounts", force: :cascade do |t|
    t.integer  "task_id",                 null: false
    t.date     "create_date",             null: false
    t.integer  "amount",      default: 0, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["task_id", "create_date"], name: "task_amounts_relationships_index", unique: true, using: :btree
    t.index ["task_id"], name: "index_task_amounts_on_task_id", using: :btree
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "identifier",                                              null: false
    t.string   "title",                                                   null: false
    t.string   "intro"
    t.decimal  "price",           precision: 8, scale: 2, default: "0.0", null: false
    t.string   "click_url",                                               null: false
    t.integer  "click_amount",                            default: 0,     null: false
    t.boolean  "has_callback",                            default: false, null: false
    t.integer  "callback_amount",                         default: 0,     null: false
    t.string   "check_url"
    t.integer  "app_id",                                                  null: false
    t.integer  "channel_id",                                              null: false
    t.integer  "admin_id",                                                null: false
    t.date     "start_at",                                                null: false
    t.date     "end_at",                                                  null: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.date     "time"
    t.index ["admin_id"], name: "index_tasks_on_admin_id", using: :btree
    t.index ["app_id"], name: "index_tasks_on_app_id", using: :btree
    t.index ["channel_id"], name: "index_tasks_on_channel_id", using: :btree
    t.index ["identifier"], name: "index_tasks_on_identifier", using: :btree
  end

  create_table "user_tasks", force: :cascade do |t|
    t.integer  "channel_id",                                             null: false
    t.integer  "task_id",                                                null: false
    t.integer  "agency_id",                                              null: false
    t.integer  "agency_task_id",                                         null: false
    t.integer  "admin_id",                                               null: false
    t.decimal  "price",          precision: 8, scale: 2, default: "0.0", null: false
    t.string   "idfa"
    t.boolean  "callbacked",                             default: false, null: false
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.integer  "app_id"
    t.date     "create_date"
    t.index ["admin_id"], name: "index_user_tasks_on_admin_id", using: :btree
    t.index ["agency_id"], name: "index_user_tasks_on_agency_id", using: :btree
    t.index ["agency_task_id"], name: "index_user_tasks_on_agency_task_id", using: :btree
    t.index ["app_id"], name: "index_user_tasks_on_app_id", using: :btree
    t.index ["callbacked"], name: "index_user_tasks_on_callbacked", using: :btree
    t.index ["channel_id"], name: "index_user_tasks_on_channel_id", using: :btree
    t.index ["idfa"], name: "index_user_tasks_on_idfa", using: :btree
    t.index ["task_id"], name: "index_user_tasks_on_task_id", using: :btree
  end

  add_foreign_key "admin_roles", "admins"
  add_foreign_key "admin_roles", "roles"
  add_foreign_key "agency_tasks", "admins"
  add_foreign_key "agency_tasks", "agencies"
  add_foreign_key "agency_tasks", "apps"
  add_foreign_key "agency_tasks", "tasks"
  add_foreign_key "role_permissions", "permissions"
  add_foreign_key "role_permissions", "roles"
  add_foreign_key "task_amounts", "tasks"
  add_foreign_key "tasks", "admins"
  add_foreign_key "tasks", "apps"
  add_foreign_key "tasks", "channels"
  add_foreign_key "user_tasks", "admins"
  add_foreign_key "user_tasks", "agencies"
  add_foreign_key "user_tasks", "agency_tasks"
  add_foreign_key "user_tasks", "apps"
  add_foreign_key "user_tasks", "channels"
  add_foreign_key "user_tasks", "tasks"
end
