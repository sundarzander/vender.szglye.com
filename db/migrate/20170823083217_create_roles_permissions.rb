class CreateRolesPermissions < ActiveRecord::Migration[5.0]
	def change
		create_table :role_permissions, id: false do |t|
			t.references :role, null: false, index: true
			t.references :permission, null: false, index: true
		end

		add_foreign_key :role_permissions, :roles
		add_foreign_key :role_permissions, :permissions
		add_index :role_permissions, [:role_id, :permission_id], unique: true
	end
end

