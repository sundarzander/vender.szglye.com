class CreateApps < ActiveRecord::Migration[5.0]
  def change
    create_table :apps do |t| 
    	t.string :title,             null: false
      t.string :image,        null: false
      t.integer :app_store_id,        null: false, index: true, unique: true
      t.string :app_store_url,        null: false
      t.integer :app_type, null: false, default: 0  # 0 iOS  1 Android  2 WP
      t.text :intro
      
      t.timestamps
    end
  end
end
