class CreateAdminRoles < ActiveRecord::Migration[5.0]
	def change
		create_table :admin_roles, id: false do |t|
			t.references :admin, null: false, index: true
			t.references :role, null: false, index: true
		end
		
		add_foreign_key :admin_roles, :admins
		add_foreign_key :admin_roles, :roles
		add_index :admin_roles, [:admin_id, :role_id], unique: true
	end
end
