class CreateAgencies < ActiveRecord::Migration[5.0]
  def change
    create_table :agencies do |t|
      t.string :name,        null: false, unique: true

      t.text :intro

      t.string :letter,              null: false
      

      t.string :key,        null: false, index: true, unique: true
      
      t.timestamps
    end
  end
end
