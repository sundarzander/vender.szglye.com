class CreatePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :permissions do |t|
      t.string :name
      t.string :resource_name, null: false # controller_name 或 action_name
      t.string :ancestry
      t.integer :ancestry_depth, null: false, default: 0  # 0 controller 1 action
      
      t.timestamps
    end
  end
end
