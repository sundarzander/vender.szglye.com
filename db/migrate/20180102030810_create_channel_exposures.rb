class CreateChannelExposures < ActiveRecord::Migration[5.0]
  def change
    create_table :channel_exposures do |t|
    	t.string :title,        null: false # 任务标题
    	t.string :app,        	null: false # app
    	t.string :cpm,        	null: false # cpm
    	t.string :channel,      null: false # 渠道
      t.string :start_at,     null: false # 开始时间
      t.string :end_at,       null: false # 结束时间
      t.references :admin,    null: false, index: true
    	t.string :price,      							# 单价

      t.timestamps
    end
  end
end
