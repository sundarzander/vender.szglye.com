class CreateSettings < ActiveRecord::Migration[5.0]
	def change
		create_table :settings do |t|
			t.string :title,null: false
			t.string :description,null: false
			t.string :domain_name,null: false

			t.timestamps null: false
		end
	end
end
