class CreateTaskAmounts < ActiveRecord::Migration[5.0]
	def change
		create_table :task_amounts do |t|
			t.references :task, null: false, index: true
			t.date :create_date, null: false
			t.integer :amount, null: false, default: 0
			
			t.timestamps null: false
		end
		
		add_foreign_key :task_amounts, :tasks
		add_index :task_amounts, [:task_id, :create_date], unique: true, name: 'task_amounts_relationships_index'
	end
end
