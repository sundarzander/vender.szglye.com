class AddCpaToAgencyTasks < ActiveRecord::Migration[5.0]
  def change
  	add_column :agency_tasks,:cpa,:integer,default: 0
  	add_column :agency_tasks,:cpc,:integer,default: 0
  	add_column :agency_tasks,:ok,:boolean,default: false
  	
  end
end
