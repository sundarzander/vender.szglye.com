class CreateTasks < ActiveRecord::Migration[5.0]
	def change
		create_table :tasks do |t|
			t.string :identifier, null: false, index: true, unique: true
      t.string :title,        null: false # 任务标题
      t.string :intro
      t.decimal :price, null: false, default: 0, :precision => 8, :scale => 2 # channel price
      
      # 点击
      t.string :click_url, null: false
      t.integer :click_amount, null: false, default: 0
      
      # 回调
      t.boolean :has_callback, null: false, default: false
      t.integer :callback_amount, null: false, default: 0
      
      # 排重
      t.string :check_url

      t.references :app, null: false, index: true # app 
      t.references :channel, null: false, index: true # channel 
      t.references :admin, null: false, index: true # admin

      # 日期区间
      t.date :start_at, null: false
      t.date :end_at, null: false

      # t.boolean :enabled, null: false, default: false
      
      t.timestamps null: false
    end
    
    add_foreign_key :tasks, :apps
    add_foreign_key :tasks, :channels
    add_foreign_key :tasks, :admins

    
  end
end
