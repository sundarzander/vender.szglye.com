class CreateUserTasks < ActiveRecord::Migration[5.0]
	def change
		create_table :user_tasks do |t|
      t.references :channel, null: false, index: true # channel 
      t.references :task, null: false, index: true # channel_task 
      t.references :agency, null: false, index: true # agency 
      t.references :agency_task, null: false, index: true # agency_task 
      t.references :admin, null: false, index: true # admin 
      
      t.decimal :price, null: false, default: 0, :precision => 8, :scale => 2 # agency price
      
      t.string :idfa, index: true
      # t.string :ip, index: true  # 参数的IP
      # t.string :mac, index: true
      # t.string :request_ip, null: false, index: true  # 请求来源的IP
      # t.jsonb :extra_data   # 其他数据

      # t.datetime :callback_at
      t.boolean :callbacked, null: false, default: false, index: true  # 已回调 渠道商那里得到通知

      # t.datetime :notify_at
      # t.string :notify_url # 代理的回调地址
      # t.boolean :notified, null: false, default: false, index: true   # 已通知 通知代理
      # t.string :notify_remark
      
      t.timestamps null: false
    end
    
    add_foreign_key :user_tasks, :channels
    add_foreign_key :user_tasks, :tasks
    add_foreign_key :user_tasks, :agencies
    add_foreign_key :user_tasks, :agency_tasks
    add_foreign_key :user_tasks, :admins
    

  end
end
