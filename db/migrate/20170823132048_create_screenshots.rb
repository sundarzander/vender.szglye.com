class CreateScreenshots < ActiveRecord::Migration[5.0]
  def change
    create_table :screenshots do |t|
    	
      t.string :image,        null: false 
      t.references :app, index: true

      t.timestamps
    end
  end
end
