class CreateAgencyTasks < ActiveRecord::Migration[5.0]
	def change
		create_table :agency_tasks do |t|
      t.decimal :price, null: false, default: 0, :precision => 8, :scale => 2 # agency price
      t.integer :amount, null: false, default: 0
      
      t.integer :click_amount, null: false, default: 0
      t.integer :callback_amount, null: false, default: 0

      t.references :task, null: false, index: true # channel_task 
      t.references :agency, null: false, index: true # agency 
      
      t.date :create_date, null: false
      t.boolean :enabled, null: false, default: false

      t.timestamps null: false
    end
    
    add_foreign_key :agency_tasks, :tasks
    add_foreign_key :agency_tasks, :agencies
    
  end
end

