class AddAdminIdToAgencyTask < ActiveRecord::Migration[5.0]
  def change
		add_reference :agency_tasks, :admin, index: true, foreign_key: true
    add_reference :agency_tasks, :app, index: true, foreign_key: true
    add_reference :user_tasks, :app, index: true, foreign_key: true

  end
end
