Rails.application.routes.draw do
  
  get '/doc' => 'portal#doc' 
  


  devise_for :admins, :controllers => {registrations: "admin/registrations", sessions: "admin/sessions", passwords: "admin/passwords"}, path: 'admin'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "admin/dashboard#index"
  authenticate :admin do
    namespace :admin do
      get '/' => 'dashboard#index'
     
      get '/setting' => 'dashboard#edit'
      post '/setting' => 'dashboard#update'
      
      resources :employees, only: [:index, :new, :create, :edit, :update, :destroy] do
        member do
          patch 'update_status' => 'users#update_status'
        end
      end
      
      resources :roles, only: [:index, :new, :create, :show, :edit, :update, :destroy]
      resources :permissions, only: [:index, :new, :create, :show, :edit, :update, :destroy]
      resources :apps, only: [:index, :new, :create, :edit, :update, :destroy] do
        resources :service_zones, only: [:index, :edit, :update, :destroy]
        member do
          patch 'remove_screenshot' => 'apps#remove_screenshot'
          delete 'clean_service_zones' => 'apps#clean_service_zones'
          patch 'update_checkin' => 'apps#update_checkin'
        end
      end

      resources :salesmen, only: [:index, :new, :create, :edit, :update]

      resources :tasks, only: [:index, :new, :create, :show, :edit, :update, :destroy] do
        resources :distributes, only: [:index] do
          collection do
            patch 'update_task' => 'distributes#update_task'
          end
        end
        resources :task_amounts, only: [:index] do
          collection do
            patch 'update_amount' => 'task_amounts#update_amount'
          end
        end
      end
      resources :channels, only: [:index, :new, :create, :edit, :update, :destroy] do
        # resources :templates, only: [:index, :new, :create, :show, :edit, :update, :destroy], controller: 'channel_templates' do
        #   member do
        #     post '/update_params' => 'channel_templates#update_params'
        #   end
        #   collection do
        #     get '/selection' => 'channel_templates#selection'
        #   end
        # end
      end

      resources :reports, only: [] do
        collection do
          get '/channel' => 'reports#channel'
          get '/user' => 'reports#user'
          get '/agency' => 'reports#agency'
          get '/channel_exposures' => 'reports#channel_exposures'
          get '/agency_exposures' => 'reports#agency_exposures'
          
          # get '/click_log' => 'reports#click_log'
          # get '/notify_log' => 'reports#notify_log'
        end
      end
      resources :agencies, only: [:index, :new, :create, :edit, :update, :destroy] do
      end
      resources :channel_exposures, only: [:index, :new, :create, :edit, :update, :destroy] do
      end
      resources :agencies_exposure, only: [:index, :new, :create, :edit, :update, :destroy] do
      end
      resources :inserts,only:[:index,:new,:create]  do
        collection do
          get '/create_data' => 'inserts#create_data'
        end
      end
      resources :imports,only:[:index,:new,:create]  do
      
      end

      
    end
  end
end