class AddUserTasksJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # Do something later
    @agency_task = AgencyTask.find_by(ok:false)
    unless @agency_task.nil?
      @agency_task.update(ok:true)
      
      cpc = @agency_task.cpc
      cpa = @agency_task.cpa
      t = [true] * cpa
      f = [false] * cpc
      array = (t + f).shuffle
      array.each do |i|
        c_date = Time.parse(@agency_task.create_date.to_s)+rand(0..86399)
        @user_task = @agency_task.user_tasks.create(
          admin_id: @agency_task.admin_id,
          channel_id: @agency_task.task.channel_id,
          agency_id: @agency_task.agency_id,
          task_id: @agency_task.task.id,
          app_id: @agency_task.app_id,
          create_date:c_date,
          created_at: c_date,
          callbacked:i 
          )
      end
    else
    	p "甜味虹"
    end
  end
end
