class Admin::SalesmenController < Admin::ApplicationController
  layout 'admin/layouts/application'
  
  before_filter :authenticate_admin!
  before_filter :find_salesman, only:[:edit, :update, :destroy]
  
  # authorize_resource
  
  def index
    @q = Salesman.search(params[:q])
    @salesmen = @q.result.paginate(:page => params[:page])
  end
  
  def new
    @salesman = Salesman.new
  end

  def edit
    
  end
  
  def create
    @salesman = Salesman.new(salesman_params)
    
    if @salesman.save
      flash[:success] = "添加成功！"
      redirect_to admin_salesmen_path
    else
      render 'new'
    end
  end
  
  def update
    if @salesman.update(salesman_params)
      flash[:success] = "修改成功！"
      redirect_to admin_salesmen_path
    else
      render 'edit'
    end
  end
  
  private
  def find_salesman
    @salesman = Salesman.find(params[:id])
    rescue Exception => exception
      redirect_to admin_salesmen_path, alert: '没有找到数据'
  end
  
  def salesman_params
    params.require(:salesman).permit(:name, :intro)
  end
  
  def generate_salesman_code
    code = [*'A'..'Z',*'0'..'9'].sample(3).join
    salesman = Salesman.where(code: code)
    if salesman.count == 0
      code
    else
      generate_salesman_code
    end
  end
end
