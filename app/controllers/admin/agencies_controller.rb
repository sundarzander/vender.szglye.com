require 'admin/application_controller'
class Admin::AgenciesController < Admin::ApplicationController
  layout 'admin/layouts/application'
  
  before_filter :find_agency, only:[:edit, :update, :destroy]
  
  def index
    
    @q = Agency.search(params[:q])
    @agencies = @q.result.paginate(:page => params[:page])
  end
  
  def new
    @agency = Agency.new
  end

  def edit
    
  end
  
  def create
    @agency = Agency.new(agency_params)
    
    if @agency.save
      flash[:success] = "添加成功！"
      redirect_to admin_agencies_path
    else
      render 'new'
    end
  end
  
  def update
    if @agency.update(agency_params)
      flash[:success] = "修改成功！"
      redirect_to admin_agencies_path
    else
      render 'edit'
    end
  end
  
  def destroy
    if @agency.agency_tasks.any?
      redirect_to admin_agencies_path, :alert => '已有接入任务，无法删除'
    else
      @agency.destroy
      redirect_to admin_agencies_path, notice: "删除成功！"
    end
  end
  
  private
  def find_agency
    @agency = Agency.find(params[:id])
    rescue Exception => exception
      redirect_to admin_agencies_path, alert: '没有找到数据'
  end
  
  def agency_params
    params.require(:agency).permit(:name, :intro)
  end
end
