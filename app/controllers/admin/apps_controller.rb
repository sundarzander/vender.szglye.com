class Admin::AppsController < Admin::ApplicationController
	layout 'admin/layouts/application'
	before_action :find_app, only:[:edit, :update, :destroy, :remove_screenshot, :clean_service_zones, :update_checkin]
  def index
    @q = App.search(params[:q])
    @apps = @q.result.order("id asc").paginate(:page => params[:page])
  end
  
  def new
    @app = App.new
  end

  def create
    # 暂时只支持iOS
    @app = App.new
    app_store_url = params[:app][:app_store_url]
    dup_app = App.find_by_app_store_url(app_store_url)
    if not app_store_url.start_with?('https://itunes.apple.com/cn/app/')
      flash.now[:alert] = "无效的App Store地址"
      render 'new'
    elsif dup_app.present?
      flash.now[:alert] = "该App已存在！"
      render 'new'
    else
      app_id = app_store_url.split('/id')[1].split('?mt=8')[0]
  		begin
  			res = RestClient.get app_store_url, :user_agent => "iPhone", :timeout => 5
	  		ActiveRecord::Base.transaction do
		  		if res.code == 200
		        @app.app_type = 0 # iOS
		        @app.app_store_url = app_store_url
		        @app.app_store_id = app_id
		  			doc = Nokogiri::HTML(res)
		  			doc.css('div.artwork > meta').each do |img_tag|
		          begin
		            @app.image.download! img_tag['content']
		          rescue
		          ensure
		          end
		  			end
		  			doc.css('h1').each do |h1_tag|
		  			  @app.title = h1_tag.content
		  			end
		  			@app.intro =  doc.css('p[itemprop="description"]').children
		  			# doc.css('p[itemprop="description"]').each do |intro|
		  			# 	@app.intro = intro.content
		  			# end
		  		end
		      
		      @app.save

		      doc.css('div.iphone-screen-shots img[itemprop="screenshot"]').each do |img_tag|
		        screenshot = @app.screenshots.build
		        begin
		          screenshot.image.download! img_tag['src']
		          screenshot.save
		        rescue
		        ensure
		        end
		      end
		      redirect_to admin_apps_path, notice: "添加完成！"
		    end
  		rescue
  			flash.now[:alert] = "连接超时，请稍后尝试！"
  			render 'new'
  		end

    end
  end

  def edit
  	
  end

  def update
  	if @app.update(app_params)
  		if params[:services].present?
	  		services = params[:services].split("/")
	  		services.each do |service|
	  			begin
	  				sz = @app.service_zones.build(name: service)
	  				sz.save
	  			rescue
	  			end
	  		end
	  	end

	  	if params[:services_file].present?
	  		file = params[:services_file]
	  		begin
		  		xls = Roo::Excelx.new(file.path)
		  		sheet = xls.sheet(0)
		  		(1..sheet.last_row).each do |i|
		  			begin
		  				sz = @app.service_zones.build(name: sheet.cell(i, 'A'))
		  				sz.save
		  			rescue
		  			end
				  end
	  		rescue
	  			redirect_to edit_admin_app_path(@app), alert: "请导入 .xlsx 格式的文件"
	  			return
	  		end
	  	end
	  	redirect_to admin_apps_path, notice: "修改成功"
	  else
	  	render 'edit'
	  end
  end

  def destroy
  	if @app.tasks.any? || @app.user_tasks.any? || @app.agency_tasks.any?
  		redirect_to admin_apps_path, alert: "该APP已关联，无法删除"
  	else
  		@app.destroy
  		redirect_to admin_apps_path, alert: "删除成功"
  	end
  end

  def remove_screenshot
  	screenshot = @app.screenshots.find_by_id(params[:screenshot_id])
  	if screenshot.destroy
  		render json: {done: true}
  	else
  		render json: {done: false}
  	end
  end

  


  private
  def find_app
    @app = App.includes(:screenshots).find(params[:id])
    rescue => e
      redirect_to admin_apps_path, alert: '没有找到数据'
  end

  def app_params
    params.require(:app).permit(:title, :image, :app_store_url, :intro, :gift_dates,screenshots_attributes: [:id, :description, :image])
  end

end
