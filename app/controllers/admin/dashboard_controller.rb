class Admin::DashboardController < Admin::ApplicationController
	layout 'admin/layouts/application'
	def index
	end
	def edit
	end
	
	def update
		if @setting.update(params.require(:setting).permit(:title, :description, :domain_name))
			redirect_to admin_path, notice: "修改成功！"
		else
			render 'edit'
		end
	end
end