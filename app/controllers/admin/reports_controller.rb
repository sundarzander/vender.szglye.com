class Admin::ReportsController < Admin::ApplicationController

	layout 'admin/layouts/application'
  	before_filter :find_admin
	def channel
		@channel_sections = Channel.pluck(:name, :id).unshift(["客户", nil])

		@app_sections = App.pluck(:title, :id).unshift(["App", nil])

		if current_admin.id == 1
			@task_sections = Task.pluck(:title, :id).unshift(["任务", nil])
			@q = Task.includes( :app).search(params[:q])
			tasks_all = @q.result
			@tasks = @q.result.order('start_at desc').paginate(:page => params[:page])
		else 
			@task_sections =current_admin.tasks.pluck(:title, :id).unshift(["任务", nil])
			@q = current_admin.tasks.includes( :channel,:app).search(params[:q])
			tasks_all = @q.result
			@tasks = @q.result.order('start_at desc').paginate(:page => params[:page])

		end

	end

	def user
		@channel_sections = Channel.pluck(:name, :id).unshift(["客户", nil])
		@agency_sections = Agency.pluck(:name, :id).unshift(["渠道", nil])

		@app_sections = App.pluck(:title, :id).unshift(["App", nil])
		if current_admin.id == 1
			@task_sections = Task.pluck(:title, :id).unshift(["任务", nil])
			@agency_sections = Agency.pluck(:name, :id).unshift(["渠道", nil])

			@q = UserTask.includes( :task,:app,:channel,:agency).search(params[:q])
			user_tasks_all = @q.result
			@user_tasks = @q.result.order('created_at desc').paginate(:page => params[:page])
		else
			@task_sections =current_admin.tasks.pluck(:title, :id).unshift(["任务", nil])

			@q = current_admin.user_tasks.includes( :task,:app,:channel,:agency).search(params[:q])
			user_tasks_all = @q.result
			@user_tasks = @q.result.order('created_at desc').paginate(:page => params[:page])
		end
	end 


	def agency
		@channel_sections = Channel.pluck(:name, :id).unshift(["客户", nil])
		@agency_sections = Agency.pluck(:name, :id).unshift(["渠道", nil])
		@app_sections = App.pluck(:title, :id).unshift(["App", nil])
		

		if current_admin.id == 1
			@task_sections = Task.pluck(:title, :id).unshift(["任务", nil])
			@q = AgencyTask.includes(:channel,:agency,:task,:app).search(params[:q])
			tasks_all = @q.result
			@agency_tasks = @q.result.order('create_date desc').paginate(:page => params[:page])
		else 
			@task_sections =current_admin.tasks.pluck(:title, :id).unshift(["任务", nil])
			@q = current_admin.agency_tasks.includes( :channel,:agency,:task,:app).search(params[:q])
			tasks_all = @q.result
			@agency_tasks = @q.result.order('create_date desc').paginate(:page => params[:page])

		end		
	end

	def channel_exposures
		if current_admin.id == 1
			@channel_exposures = ChannelExposure.all
		else
			@channel_exposures = current_admin.channel_exposures
		end
	end

	def agency_exposures
		if current_admin.id == 1
			@agency_exposures = AgencyExposure.all
		else
			@agency_exposures = current_admin.agency_exposures
		end
	end

	private
	def find_admin

		if current_admin.id == 1
			@admin_sections = Admin.pluck(:username,:id).unshift(['公司',nil])
		else
			@admin_sections = Admin.where(id: current_admin.id).pluck(:username,:id).unshift(['公司',nil])
		end
	end

end
