class Admin::ChannelsController < Admin::ApplicationController
  layout 'admin/layouts/application'
  
  before_filter :find_channel, only:[:edit, :update, :destroy]
  
  def index
    @q = Channel.search(params[:q])
    @channels = @q.result.paginate(:page => params[:page])
  end
  
  def new
    @channel = Channel.new
  end

  def edit
    
  end
  
  def create
    @channel = Channel.new(channel_params)
    
    if @channel.save
      redirect_to admin_channels_path
    else
      render 'new'
    end
  end
  
  def update
    if @channel.update(channel_params)
      flash[:success] = "修改成功！"
      redirect_to admin_channels_path
    else
      render 'edit'
    end
  end
  
  def destroy
    if @channel.tasks.any?
      redirect_to admin_channels_path, :alert => '已有接入任务，无法删除'
    else
      @channel.destroy
      redirect_to admin_channels_path, notice: "删除成功！"
    end    
  end
  
  private
  def find_channel
    @channel = Channel.find(params[:id])
    rescue Exception => exception
      redirect_to admin_channels_path, alert: '没有找到数据'
  end
  
  def channel_params
    params.require(:channel).permit(:name, :intro)
  end
end
