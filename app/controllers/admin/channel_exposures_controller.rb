class Admin::ChannelExposuresController < Admin::ApplicationController
  layout 'admin/layouts/application'
   

  def index
    @channel_exposures = ChannelExposures.all
  end

  def new
    
  end

  def edit
  end

  def create
    @channel_exposures = ChannelExposures.new(channelexposures_params)
    
    if @channel_exposures.save
      redirect_to admin_channelexposures_path
    else
      render 'new'
    end 
  end

  def update
  end

  def destroy
  end

  
end
