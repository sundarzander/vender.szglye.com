class Admin::RolesController < Admin::ApplicationController
  layout 'admin/layouts/application'
  before_action :find_role, only:[:edit, :update, :destroy]
  before_action :find_permissions, only: [:new, :create, :edit, :update]

  def index
  	@roles = Role.includes(:admins, :permissions).all
  end

  def new
  	@role = Role.new
  end

  def create
    @role = Role.new(role_params)
    if @role.save
    	if params[:ids].present?
        params[:ids].each do |id|
          permission = Permission.find_by_id(id)
          @role.permissions << permission
        end
      end
      redirect_to admin_roles_path, notice: "添加成功！"
    else
      render 'new'
    end
  end

  def edit
  	@permission_ids = @role.permission_ids
  end

  def update
    if @role.update(role_params)
    	if params[:ids].present?
        param_ids = params[:ids].map {|id| id.to_i}
        permission_ids = @role.permission_ids
        remove_ids = permission_ids - param_ids
        param_ids.each do |id|
          permission = Permission.find_by_id(id)
          @role.permissions << permission unless @role.permissions.exists?(permission)
        end
        remove_ids.each do |id|
          permission = Permission.find_by_id(id)
          @role.permissions.destroy(permission)
        end
      else
      	@role.permissions.clear
      end
      redirect_to admin_roles_path, notice: "修改成功！"
    else
      render 'edit'
    end
  end
  
  def destroy
		if @role.admins.present?
			redirect_to admin_roles_path, alert: "该角色已关联管理员，无法删除！"
		else
			@role.destroy
	  	redirect_to admin_roles_path, notice: "删除成功！"
		end
  end



  private
  def find_role
    @role = Role.find(params[:id])
    rescue => e
      redirect_to admin_roles_path, alert: '没有找到数据'
  end
  
  def find_permissions
  	@permissions = []
  	permissions = Permission.where("ancestry is NULL")
  	permissions.each do |permission|
  		p = {}
  		p["controller"] = [permission.id, permission.name]
  		permission_id_and_name = permission.children.pluck(:id, :name)
  		p["action"] = permission_id_and_name
  		@permissions << p
  	end
  end

  def role_params
    params.require(:role).permit(:name, permissions_attributes: [:id, :name, :resource_name])
  end
  
end
