class Admin::ImportsController < ApplicationController
	layout 'admin/layouts/application'
	
	def index

	end 
	
	def new
		@channel_exposure = ChannelExposure.new
	end

	def create
		@channel_exposure = ChannelExposure.new
		errors = []
    if params[:tasks_file].present?
      file = params[:tasks_file]
      if file.original_filename.split('.').last == 'xlsx'
        xls = Roo::Excelx.new(file.path)
        ActiveRecord::Base.transaction do   #判断回滚
          begin
        	sheet1 =  xls.sheet(0)
        	(2..sheet1.last_row).each do |cell|    #遍历
              title = sheet1.cell("a",cell)
              app = sheet1.cell("b",cell)
              channel = sheet1.cell("c",cell)
              cpm = sheet1.cell("d",cell) 
              cycle = sheet1.cell("e",cell).split("-")
              start_at = cycle[0]
              end_at = cycle[1]                        
              @channel_exposure = ChannelExposure.find_by(cpm:cpm,title:title,app:app,start_at:start_at,end_at:end_at)
              unless @channel_exposure
                @channel_exposure = current_admin.channel_exposures.create(title:title,app:app,channel:channel,cpm:cpm,start_at:start_at,end_at:end_at,admin_id: current_admin.id)
              end  
          	end
            sheet2 =  xls.sheet(1)
            (2..sheet2.last_row).each do |cell|    #遍历
                title = sheet2.cell("a",cell)
                app = sheet2.cell("b",cell)
                channel = sheet2.cell("c",cell)
                agency = sheet2.cell("d",cell)
                cpm = sheet2.cell("e",cell) 
                start_at = sheet2.cell("f",cell)
                @agency_exposure = AgencyExposure.find_by(cpm:cpm,title:title,app:app,start_at:start_at)
                unless @agency_exposure
                  @agency_exposure = current_admin.agency_exposures.create(title:title,app:app,agency:agency,channel:channel,cpm:cpm,start_at:start_at,admin_id: current_admin.id)
                end  
              end
            rescue Exception => e
            errors << "表格数据有误！"
            raise ActiveRecord::Rollback
          end       
      	end
      end
      if errors.length ==  0       
      	redirect_to admin_imports_path, notice: "导入成功！"
      else
      	redirect_to admin_imports_path, notice: "上传有误，请检查表格重新上传！"
     	end
    end
  end
end
