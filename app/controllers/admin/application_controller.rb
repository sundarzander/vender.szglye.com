class Admin::ApplicationController < ActionController::Base
  layout 'admin/layouts/application'
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # before_action :authenticate_admin!
  before_filter :authenticate_admin!
  before_filter :find_setting
  before_action :check_permission
  
  def find_setting
    @setting = Setting.first
  end

  def check_permission
    # 把没有纳入 permission 的纳入，顺便检查权限。
    permission_controller = Permission.find_by_resource_name(controller_name)
    unless permission_controller
      permission_controller = Permission.new(name: controller_name, resource_name: controller_name)
      if permission_controller.save
        permission_action = permission_controller.children.new(name: action_name, resource_name: action_name)
        permission_action.save
      end
    else
      permission_action = permission_controller.children.find_by_resource_name(action_name)
      unless permission_action
        permission_action = permission_controller.children.new(name: action_name, resource_name: action_name)
        permission_action.save
      end
    end
    
    unless current_admin.id == 1
      has_permission = false
      current_admin.roles.each do |role|
        controller = role.permissions.where("ancestry is NULL").where(resource_name: controller_name).first
        if controller.present?
          action = role.permissions.where(ancestry: controller.id, resource_name: action_name).first
          if action
            has_permission = true
            break
          end
        end
      end
      unless has_permission
        redirect_to admin_path, alert: "您没有权限访问该页面！"
        return
      end
    end

  end
end