
class Admin::EmployeesController < Admin::ApplicationController
  layout 'admin/layouts/application'
  
  before_action :find_admin, only:[:edit, :update, :update_status, :destroy]
  before_action :find_roles, only:[:new, :create, :edit, :update]

  def index
    @admins = Admin.includes(:roles).all
  end
  
  def new
    @admin = Admin.new
  end

  def edit
    @role_ids = @admin.role_ids
  end
  
  def create
    @admin = Admin.new(admin_params)
    if @admin.save
      if params[:role_ids].present?
        params[:role_ids].each do |id|
          role = Role.find_by_id(id)
          @admin.roles << role
        end
      end
      redirect_to admin_employees_path, notice: "添加成功！"
    else
      render 'new'
    end
  end
  
  def update
    if @admin.id != 1 || current_admin.id == 1
      update_params = params.require(:admin).permit(:password) 
      if @admin.update(update_params)
        if params[:role_ids].present?
          param_ids = params[:role_ids].map {|id| id.to_i}
          role_ids = @admin.role_ids
          remove_ids = role_ids - param_ids
          param_ids.each do |id|
            role = Role.find_by_id(id)
            @admin.roles << role unless @admin.roles.exists?(role)
          end
          remove_ids.each do |id|
            role = Role.find_by_id(id)
            @admin.roles.destroy(role)
          end
        else
          @admin.roles.clear
        end
        redirect_to admin_employees_path, notice: "修改成功！"
      else
        render 'edit'
      end
    else
      redirect_to admin_employees_path, :alert => 'id 1 管理员不能修改'
    end
  end
  
  def destroy
    if @admin.id == 1
      redirect_to admin_employees_path, :alert => 'id 1 管理员不能删除'
    else
      @admin.destroy
      redirect_to admin_employees_path, notice: "删除成功！"
    end    
  end
  
  private
  def find_admin
    @admin = Admin.find(params[:id])
    rescue => e
      redirect_to admin_employees_path, alert: '没有找到数据'
  end
  
  def find_roles
    @roles = Role.all
  end

  def admin_params
    params.require(:admin).permit(:username, :email, :password)
  end

end
