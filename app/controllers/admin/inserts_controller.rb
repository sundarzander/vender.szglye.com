class Admin::InsertsController  < Admin::ApplicationController
  layout 'admin/layouts/application'
  def index
  end 
  def new
    @task = Task.new
  end


  def create_data
    @agency_task = AgencyTask.find_by(ok:false)
    unless @agency_task.nil?
      @agency_task.update(ok:true)

      cpc = @agency_task.cpc
      cpa = @agency_task.cpa
      t = [true] * cpa
      f = [false] * cpc
      array = (t + f).shuffle
      array.each do |i|
        c_date = Time.parse(@agency_task.create_date.to_s)+rand(0..86399)
        @user_task = @agency_task.user_tasks.create(
          admin_id: @agency_task.admin_id,
          channel_id: @agency_task.task.channel_id,
          agency_id: @agency_task.agency_id,
          task_id: @agency_task.task.id,
          app_id: @agency_task.app_id,
          create_date:c_date,
          created_at: c_date,
          callbacked:i 
          )
      end
      redirect_to admin_inserts_path, notice: "生成完成#{array.length}"
    else
      redirect_to admin_inserts_path,notice: "没有需要生成的数据"
    end
  end

  def create
    @task = Task.new  
    errors = []
    if params[:tasks_file].present?
      file = params[:tasks_file]
      if file.original_filename.split('.').last == 'xlsx'
        xls = Roo::Excelx.new(file.path)
        ActiveRecord::Base.transaction do   #判断回滚
          begin


            sheet1 =  xls.sheet(0)
            url = "https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1504631722&di=f5f4d0ee9b583439394ebb34a0237448&src=http://file.quweiwu.com/news/20150730112552425.jpg"

            (2..sheet1.last_row).each do |cell|    #遍历
              channel = sheet1.cell("b",cell)
              app = sheet1.cell("c",cell)
              cycle = sheet1.cell("d",cell).split("-")
              start_at = cycle[0]
              end_at = cycle[1]
              callback_amount = sheet1.cell("f",cell)
              price = sheet1.cell("g",cell)
              time = sheet1.cell("a",cell)
              title = app + "(" + time.to_s + ")"
              #查找渠道，app是否已经存在，如果数据库中没有这个数据，新按数据新建
              @channel = Channel.find_by(name: channel)
              @app = App.find_by(title: app)
              unless @channel
                @channel = Channel.create(name:channel)
              end
              unless @app
                @app = App.create(title:app,app_store_url:"test.ecloudtest.com",app_store_id:1,remote_image_url:url)
              end
              #新建任务
              @task = current_admin.tasks.create(title:title,price:price,start_at:start_at,end_at:end_at,channel_id:@channel.id,app_id:@app.id,click_url:"https://test.ecloudtest.com",time:time,callback_amount:callback_amount)
              # @task.save!
              #给任务分配每天的任务量，暂时每天设为10万吧，以防万一，后面有更好的实现方法再来实现
              date_ranges = (@task.start_at..@task.end_at).to_a
              date_ranges.each do |date|
                if @task.task_amounts.where('create_date = ?', date).count == 0
                  @task.task_amounts.create(
                    task_id: @task.id,
                    amount: 100000,
                    create_date: date
                    )
                end
              end
              
            end      #遍历结束
            #以上表一几乎没有可用价值了 

            sheet2 = xls.sheet(1)
            (2..sheet2.last_row).each do |cell|    #遍历
              channel = channel = sheet2.cell("b",cell)
              agency = sheet2.cell("c",cell)
              app = sheet2.cell("d",cell)
              cycle = sheet2.cell("e",cell).split("-")
              start_at = cycle[0]
              end_at = cycle[1]
              task_amounts = sheet2.cell("g",cell)   #回调数
              turnover = sheet2.cell("i",cell)       #转换率
              f = sheet2.cell("f",cell)              #cpa,cpc
              price = sheet2.cell("h",cell)
              @agency = Agency.find_by(name:agency)
              
              unless @agency
                @agency = Agency.create(name:agency)   
              end
              @channel = Channel.find_by(name: channel)
              @app = App.find_by(title: app)
              @task = Task.find_by(app_id:@app.id,channel_id:@channel.id,start_at:start_at,end_at:end_at)
              
              #转化总数   #点击总数
              if f == "CPA"
                cpa = task_amounts               
                cpc = (cpa / turnover)              
              else
                cpa = (task_amounts * turnover)      
                cpc = task_amounts + cpa
              end
              if @task.start_at == @task.end_at      #当任务只有一天时
                @agency_task = @task.agency_tasks.create(task_id: @task.id,agency_id: @agency.id,price:price,click_amount: cpc,app_id:@app.id,admin_id: current_admin.id,create_date:@task.start_at,cpa: cpa,cpc: cpc-cpa     #此处的cpc是指无效点击
                  )
              else                                 #当任务不只有一天时
                date_ranges = (@task.start_at..@task.end_at).to_a    #任务周期
                task_count = date_ranges.length                      #任务的天数
                click_amounts = cpc - cpa         #无效点击总数
                cpa_deuce = (cpa * 0.9 / task_count)          #先转化取9成平分
                click_deuce = (click_amounts * 0.9 / task_count)    #无效点击 取9成平分
                cpa_re = (cpa * 0.1)                   #把其中 1 成 随机分配给每一天
                click_re = (click_amounts * 0.1)          
                cpa_arr = []                            #加上平均的部分生成数组，回调
                click_arr = []                                #每日点击数，无回调部分
                (1..task_count).each do |i|
                  if i < task_count
                    cpa_num = (rand() * cpa_re / 2).to_i           #生成随机的cpc数量
                    cpa_re -= cpa_num                             
                    cpa_arr << cpa_num.to_i + cpa_deuce.to_i  
                    click_num = (rand() * click_re / 2).to_i 
                    click_re -= click_num
                    click_arr << click_num.to_i + click_deuce.to_i
                  else
                    click_arr << click_re.to_i + click_deuce.to_i
                    cpa_arr << cpa_re.to_i + cpa_deuce.to_i 
                  end 
                end
                #以上生成每天的任务量，回调部分

                date_ranges.each_with_index do |date,index|
                  @agency_task = @task.agency_tasks.create(task_id: @task.id,agency_id: @agency.id,price:price,click_amount: cpa_arr[index]+click_arr[index],app_id:@app.id,admin_id: current_admin.id,create_date:date,cpa: cpa_arr[index],cpc: click_arr[index]     
                  #此处的cpc是指无效点击
                  )
                end
              end



            end #遍历结束 
          rescue Exception => e
            errors << "表格数据有误！"
            raise ActiveRecord::Rollback
          end
        end  #回滚   
        if errors.length ==  0       
          redirect_to admin_inserts_path, notice: "导入成功！"
        else
          redirect_to admin_inserts_path, notice: "上传有误，请检查表格重新上传！"
        end
      else
        flash.now['alert'] = "请添加xlsx格式的文件！"
        render 'new'
      end
    else
      flash.now['alert'] = "请添加文件！"
      render 'new'
    end
  end

end