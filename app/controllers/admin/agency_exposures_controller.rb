class Admin::AgencyExposuresController < Admin::ApplicationController
	layout 'admin/layouts/application'
	def index
	  @agency_exposure = AgencyExposures.all
	end

	def new
	  
	end

	def edit
	end

	def create
	  @agency_exposure = AgencyExposures.new(agency_exposures_params)
	  
	  if @agency_exposures.save
	    redirect_to admin_channelexposures_path
	  else
	    render 'new'
	  end 
	end

	def update
	end

	def destroy
	end
end
