class Admin::DistributesController < Admin::ApplicationController
  layout 'admin/layouts/application'
  before_filter :find_task
  
  def index
    if Agency.count == 0
      redirect_to new_admin_agency_path, alert: '请先添加代理'
      return
    end
    
    unless (@task.start_at..@task.end_at).include?(Date.today)
      redirect_to admin_tasks_path, alert: '任务未开始或已过期'
      return
    end
    
    @task_amount = @task.task_amounts.where('create_date = ?', Date.today).first
    
    if @task_amount && @task_amount.amount > 0
      ActiveRecord::Base.transaction do
        added_agency_ids = @task.agency_tasks.where('create_date = ?', Date.today).map{ |at| at.agency_id }
        if added_agency_ids.count == 0 
          @agencies = Agency.all
        else
          @agencies = Agency.where('id not in (?)', added_agency_ids)
        end
        @agencies.each do |agency|
          AgencyTask.create(
            amount: 0,
            task_id: @task.id,
            agency_id: agency.id,
            create_date: Date.today,
            admin_id: current_admin.id,
            app_id: @task.app.id
          )
        end
      end
      
      @q = @task.agency_tasks.includes(:agency, :task).search(params[:q])
      @agency_tasks = @q.result.where('create_date = ?', Date.today)
    else
      redirect_to admin_task_path(@task), alert: '请先配置任务量'
    end
  end
  
  def update_task
    amount = 0  
    @task_amount = @task.task_amounts.where('create_date = ?', Date.today).first
  
    if params[:task][:agency_tasks_attributes]
      params[:task][:agency_tasks_attributes].each do |k,v|
        amount += v[:amount].to_i
      end
      if amount > @task_amount.amount
        redirect_to admin_task_distributes_path(@task), alert: '分配的任务量不能大于今日任务总量'
        return
      end
    end
    
    if @task.update(task_params)
      flash[:success] = "设置成功！"
    end
    redirect_to admin_task_distributes_path(@task)
  end

  private
  def find_task
    @task = Task.find(params[:task_id])
    rescue Exception => exception
      redirect_to admin_tasks_path, alert: '没有找到数据'
  end
  
  def task_params
    params.require(:task).permit(agency_tasks_attributes:[:amount, :price, :id,:app_id,:admin_id])
  end
end
