class Admin::TaskAmountsController < Admin::ApplicationController
  layout 'admin/layouts/application'
  before_filter :find_task
  
  def index
    date_ranges = (@task.start_at..@task.end_at).to_a
    date_ranges.each do |date|
      if @task.task_amounts.where('create_date = ?', date).count == 0
        @task.task_amounts.create(
          task_id: @task.id,
          amount: 0,
          create_date: date
        )
      end
    end
  end
  
  def update_amount
    if @task.update(task_params)
      flash[:success] = "设置成功！"
    end
    redirect_to admin_tasks_path
  end

  private
  def find_task
    @task = Task.find(params[:task_id])
    rescue Exception => exception
      redirect_to admin_tasks_path, alert: '没有找到数据'
  end
  
  def task_params
    params.require(:task).permit(task_amounts_attributes:[:amount, :id])
  end
end

