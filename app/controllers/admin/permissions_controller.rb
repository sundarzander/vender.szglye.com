
class Admin::PermissionsController < Admin::ApplicationController
  layout 'admin/layouts/application'
  
  before_action :find_permission, only:[:edit, :update, :destroy]

  def index
    @ancestry_options = Permission.where("ancestry is null").order("id asc").pluck(:name, :id).unshift(["请选择", nil])
    @q = Permission.search(params[:q])
    @permissions = @q.result.order("ancestry desc, created_at desc").paginate(:page => params[:page])
  end

  def new
  	@permission = Permission.new
  end

  def create
    # @permission = Permission.new(permission_params)
    # if @permission.save
    #   redirect_to admin_permissions_path, notice: "添加成功！"
    # else
    #   render 'new'
    # end
  end

  def edit
  	
  end

  def update
    if @permission.update(permission_params)
      redirect_to admin_permissions_path, notice: "修改成功！"
    else
      render 'edit'
    end
  end
  
  def destroy
    # @permission.destroy
    # redirect_to admin_permissions_path, notice: "删除成功！"
  end


  private
  def find_permission
    @permission = Permission.find(params[:id])
    rescue => e
      redirect_to admin_permissions_path, alert: '没有找到数据'
  end
  
  def permission_params
    params.require(:permission).permit(:name)
  end
  
end
