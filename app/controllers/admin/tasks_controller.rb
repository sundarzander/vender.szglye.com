class Admin::TasksController < Admin::ApplicationController
  layout 'admin/layouts/application'
  before_filter :find_task, only:[:show, :edit, :update, :destroy, :update_enabled]
  before_filter :find_other, only:[:index, :new, :create, :edit, :update]

  
  def index
    @task_sections = Task.pluck(:title, :id).unshift(["请选择任务", nil])
    @q = Task.includes( :app).search(params[:q])
    @tasks = @q.result.order('start_at desc').paginate(:page => params[:page])
  end
  
  def show

  end
  
  def new
    unless Channel.any?
      redirect_to new_admin_channel_path, alert: '请先添加渠道'
      return
    end

    unless App.any?
      redirect_to new_admin_app_path, alert: '请先添加App'      
      return
    end

    @task = current_admin.tasks.build()
    @task.app_id = params[:app_id] if params[:app_id]
    @task.admin_id = params[:admin_id] if params[:admin_id]
    @task.channel_id = params[:channel_id] if params[:channel_id]

  end

  def edit

  end

  def create
    # @task = Task.new(task_params)
    @task = current_admin.tasks.build(task_params)
    
    if @task.save
      flash[:success] = "添加成功，请设置任务量！"
      redirect_to admin_task_task_amounts_path(@task)
    else
      render 'new'
    end
  end

  def update
    if @task.update(task_params)
      flash[:success] = "修改成功！"
      redirect_to admin_tasks_path
    else
      render 'edit'
    end
  end

  def destroy

  end



  private
  def find_task
    @task = Task.find(params[:id])
  rescue Exception => exception
    redirect_to admin_tasks_path, alert: '没有找到数据'
  end

  def find_other
    @app_sections = App.pluck(:title, :id).unshift(["请选择App", nil])
    @channel_sections = Channel.pluck(:name,:id).unshift(["请选择渠道",nil])
  end

  def task_params
    params.require(:task).permit(:title, :intro, :price, :click_url,  :app_id, :channel_id,  :admin_id, :start_at, :end_at, :time)
  end
end
