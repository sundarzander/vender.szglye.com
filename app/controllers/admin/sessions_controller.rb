class Admin::SessionsController < Devise::SessionsController
   layout 'admin/layouts/application'
  def new
    super
  end
  
  def create
    super
  end
  
  def destroy
    super
  end
  
  protected
  def after_sign_in_path_for(resource)
    admin_path
  end
  def after_sign_out_path_for(resource)
    # root_path
    new_admin_session_path
  end
end 