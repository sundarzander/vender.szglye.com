class ChannelExposuresController < ApplicationController
  layout 'admin/layouts/application'
  before_filter :find_channel, only:[:edit, :update, :destroy]
  def index
  end

  def new
    @channelexposures = ChannelExposures.new
  end

  def edit
  end

  def create
    @channel = Channel.new(channel_params)
    
    if @channel.save
      redirect_to admin_channels_path
    else
      render 'new'
    end
  end

  def update
  end

  def destroy
  end
end
