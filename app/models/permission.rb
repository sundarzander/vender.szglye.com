class Permission < ApplicationRecord
	 has_and_belongs_to_many :roles, :join_table => :role_permissions
  # accepts_nested_attributes_for :roles

  # tree structure
  acts_as_tree cache_depth: true
  
  validates :resource_name, presence: true
end
