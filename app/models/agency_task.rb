class AgencyTask < ApplicationRecord
	belongs_to :channel
  belongs_to :agency
  belongs_to :task
  belongs_to :admin
  belongs_to :app
  has_many :user_tasks  

  validates :agency_id, presence: {:message => '必须选择'}
  validates :task_id, presence: {:message => '必须选择'}
end
