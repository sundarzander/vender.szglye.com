class App < ApplicationRecord
	mount_uploader :image, AppImageUploader
  has_many :tasks
  has_many :user_tasks
  has_many :agency_tasks
	has_many :screenshots, :dependent => :destroy
	accepts_nested_attributes_for :screenshots
	
end
