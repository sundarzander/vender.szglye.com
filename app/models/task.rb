class Task < ApplicationRecord
	belongs_to :app
  # belongs_to :salesman
  belongs_to :admin
  belongs_to :channel
  has_many :agency_tasks
  accepts_nested_attributes_for :agency_tasks
  has_many :user_tasks
  has_many :task_amounts, dependent: :destroy
  accepts_nested_attributes_for :task_amounts, :allow_destroy => true
  
  validates :title, presence: true
  validates :click_url, format:{:with => /(http|https):\/\//ix, :message => '无效'}
  # validates :check_url, format:{:with => /(http|https):\/\//ix, :message => '无效'}
  
  validates :app_id, presence: {:message => '必须选择'}

  validates :start_at, presence: {:message => '必须选择'}
  validates :end_at, presence: {:message => '必须选择'}  
  validates :admin_id, presence: {:message => '必须选择'}
  


  validates_uniqueness_of :identifier
  
  before_validation :auto_identifier, on: :create
  
  protected
  def auto_identifier
    self.identifier = SecureRandom.urlsafe_base64
  end
end
