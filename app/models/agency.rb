class Agency < ApplicationRecord
	has_many :agency_tasks
	has_many :user_tasks

	validates :name, presence: true, uniqueness: true
	before_validation :auto_fill, on: :create

	protected
	def auto_fill
		if self.name.present?
			pinyin = ::PinYin.of_string(self.name, :ascii)
			self.letter = pinyin[0][0].upcase
			self.key = SecureRandom.urlsafe_base64
			
		end
	end
end
