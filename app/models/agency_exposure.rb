class AgencyExposure < ApplicationRecord
	belongs_to :admin
	validates :title, presence: true
	validates :app, presence: true
	validates :channel, presence: true
	validates :cpm, presence: true
end
