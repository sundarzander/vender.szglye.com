class Role < ApplicationRecord
	has_and_belongs_to_many :permissions, :join_table => :role_permissions
  accepts_nested_attributes_for :permissions
  
  has_and_belongs_to_many :admins, :join_table => :admin_roles
  accepts_nested_attributes_for :admins
  
  validates :name, presence: true
end
