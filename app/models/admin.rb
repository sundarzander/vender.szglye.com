class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:email]
         
  has_and_belongs_to_many :roles, :join_table => :admin_roles
  accepts_nested_attributes_for :roles
  has_many :tasks
  has_many :channel_exposures
  has_many :agency_exposures
  has_many :user_tasks
  has_many :agency_tasks
  before_validation :auto_email, on: :create
  protected
  
  def auto_email
    if self.email.blank?
    	self.email = [*'a'..'z',*'0'..'9',*'A'..'Z'].sample(10).join + '@ebunet.net'
    end
  end
end
