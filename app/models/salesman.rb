class Salesman < ApplicationRecord
	validates :name, presence: true
  
  # has_many :tasks
  
  default_scope {order('id desc')}
  
  before_save :set_first_letter
  
  def set_first_letter
    pinyin = ::PinYin.of_string(self.name, :ascii)
    self.letter = pinyin[0][0].upcase
  end
end

