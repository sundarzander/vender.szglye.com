class TaskAmount < ApplicationRecord
	belongs_to :task
    
  default_scope {order('create_date asc')}
end
