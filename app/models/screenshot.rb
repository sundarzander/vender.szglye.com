class Screenshot < ApplicationRecord
	belongs_to :app

	mount_uploader :image, AppImageUploader
end

