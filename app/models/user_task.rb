class UserTask < ApplicationRecord
  belongs_to :channel 
  belongs_to :agency
  belongs_to :task
  belongs_to :agency_task
  belongs_to :app
  belongs_to :admin
  validates :channel_id, presence: true
  validates :task_id, presence: true
  validates :agency_id, presence: true
  validates :agency_task_id, presence: true
  validates :admin_id, presence: true
  before_create do
    self.idfa = UUIDTools::UUID.timestamp_create().to_s
  end
        

end
