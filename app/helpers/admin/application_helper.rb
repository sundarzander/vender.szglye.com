module Admin::ApplicationHelper
  def main_nav
    nav_html = %Q[
      <li class="#{'active' if controller_name == 'dashboard' || controller_name == 'employees' }">#{link_to '控制面板', admin_path}</li>
      <li class="#{'active' if controller_name == 'apps'  }">#{link_to '应用管理', admin_apps_path}</li>

      <li class="#{'active' if controller_name == 'tasks' || controller_name== 'task_amounts'  }">#{link_to '任务管理', admin_tasks_path}</li>
      <li class="#{'active' if ['channels', 'channel_templates'].include? controller_name}">#{link_to '客户', admin_channels_path}</li>
      <li class="#{'active' if ['agencies'].include? controller_name}">#{link_to '渠道', admin_agencies_path}</li>
      <li class="#{'active' if ['reports'].include? controller_name}">#{link_to '报表', channel_admin_reports_path}</li>
      <li class="#{'active' if ['inserts'].include? controller_name}" style="display:none;">#{link_to '导入', admin_inserts_path}</li>
      <li class="#{'active' if ['imports'].include? controller_name}" style="display:none;">#{link_to '导入cpm', admin_imports_path}</li>   
    ]  
    nav_html
  end
  def side_nav
    if ['apps', 'gifts', 'service_zones', 'gift_codes','checkin_gifts'].include? controller_name
      nav_html = %Q[
        <li class="#{'active' if controller_name == 'apps' || controller_name == 'service_zones' || controller_name == 'checkin_gifts'}">#{link_to "APP列表", admin_apps_path}</li>
        
      ]
    elsif ['reports'].include? controller_name
      nav_html = %Q[
        <li class="#{'active' if controller_name == 'reports' && action_name == 'channel'}">#{link_to "客户报表", channel_admin_reports_path}</li>
        <li class="#{'active' if controller_name == 'reports' && action_name == 'agency'}">#{link_to "渠道报表", agency_admin_reports_path}</li>
        <li class="#{'active' if controller_name == 'reports' && action_name == 'channel_exposures'}">#{link_to "cpm 客户报表", channel_exposures_admin_reports_path}</li>
        <li class="#{'active' if controller_name == 'reports' && action_name == 'agency_exposures'}">#{link_to "cpm 渠道报表", agency_exposures_admin_reports_path}</li>
      ]
    elsif  controller_name== 'salesmen'
      nav_html = %Q[

        <li class="#{'active' if controller_name == 'salesmen'}">#{link_to "业务员列表",admin_salesmen_path}</li>

      ]
    elsif controller_name == 'tasks' || controller_name== 'task_amounts'
      nav_html = %Q[
        <li class="#{'active' if controller_name == 'tasks' || controller_name == 'task_amounts'}">#{link_to "任务列表",admin_tasks_path}</li>
      ]
    elsif ['channels', 'channel_templates'].include? controller_name
      nav_html = %Q[
        <li class="#{'active' if ['channels', 'channel_templates'].include? controller_name}">#{link_to "客户列表", admin_channels_path}</li>
      ]
    elsif ['agencies'].include? controller_name
      nav_html = %Q[
        <li class="#{'active' if ['agencies', 'agency_tasks'].include? controller_name}">#{link_to "渠道列表", admin_agencies_path}</li>
      ]
    elsif controller_name == 'inserts'
      nav_html = %Q[
        <li class="#{'active' if controller_name == 'inserts' }">#{link_to "导入数据",admin_inserts_path}</li>
      ]
    elsif controller_name == 'imports'
      nav_html = %Q[
        <li class="#{'active' if controller_name == 'imports' }">#{link_to "导入数据",admin_imports_path}</li>
      ]
    else 
      nav_html = %Q[
        <li class="#{'active' if controller_name == 'dashboard'}">#{link_to "控制面板", admin_path}</li>
        <li class="#{'active' if controller_name == 'employees'}">#{link_to "管理员列表", admin_employees_path}</li>
        <li class="#{'active' if controller_name == 'roles'}">#{link_to "角色列表", admin_roles_path}</li>
        <li class="#{'active' if controller_name == 'permissions'}">#{link_to "权限列表", admin_permissions_path}</li>
      ]
    end

    nav_html
  end
end